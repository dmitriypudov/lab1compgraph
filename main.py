import sys
import signal

from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QTextBrowser, QShortcut
import requests
from bs4 import BeautifulSoup
from PyQt5.QtCore import QObject, QTimer, pyqtSignal


class WashingtonPostScraper:
    def scrape(self):
        ret = requests.get("https://www.washingtonpost.com/world/")
        soup = BeautifulSoup(ret.text, 'html.parser')
        article = soup.find('article')
        title = article.find('h3').text.strip() if article and article.find('h3') else "Title not found"
        annotation = article.find('p').text.strip() if article else "Annotation not found"
        authors = article.find('span', class_='wpds-c-knSWeD')
        author_list = [span.text.strip() for span in authors] if authors else ["Author not found"]
        author_string = ", ".join(author_list).replace(',', '').replace('.', '')
        return {'Title': title, 'Annotation': annotation, 'Authors': author_string}

class NewYorkTimesScraper:
    def scrape(self):
        ret = requests.get("https://www.nytimes.com/section/politics")
        soup = BeautifulSoup(ret.text, 'html.parser')
        article = soup.find('article')
        title = article.find('h3').text.strip() if article and article.find('h3') else "Title not found"
        annotation = article.find('p').text.strip() if article else "Annotation not found"
        authors = article.find('span', class_='css-9voj2j')
        author_list = [span.text.strip() for span in authors] if authors else ["Author not found"]
        author_string = ", ".join(author_list).replace(',', '').replace('.', '')
        return {'Title': title, 'Annotation': annotation, 'Authors': author_string}

class TimeTimesScraper:
    def scrape(self):
        ret = requests.get("https://time.com/section/world/")
        soup = BeautifulSoup(ret.text, 'html.parser')
        articles = soup.find_all('div', class_='component taxonomy-related-touts section-related__touts')
        result = []
        for article in articles:
            title = article.find('h2').text.strip() if article.find('h2') else "Title not found"
            annotation = article.find('h3').text.strip() if article.find('h3') else "Annotation not found"
            authors = article.find('span', class_='byline')
            author_list = [span.text.strip() for span in authors] if authors else ["Author not found"]
            author_string = ", ".join(author_list).replace(',', '').replace('.', '')
            result.append({'Title': title, 'Annotation': annotation, 'Authors': author_string})
        return result

class NewsUpdater(QObject):
    news_data_ready = pyqtSignal(object)

    def __init__(self, scraper, interval):
        super().__init__()
        self.scraper = scraper
        self.interval = interval
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.background_task)

    def background_task(self):
        data = self.scraper.scrape()
        self.news_data_ready.emit(data)

    def start(self):
        self.timer.start(self.interval * 1000)
        self.background_task()

class NewsApp(QMainWindow):
    def __init__(self):
        super().__init__()

        self.washington_post_scraper = WashingtonPostScraper()
        self.ny_times_scraper = NewYorkTimesScraper()
        self.time_times_scraper = TimeTimesScraper()

        self.init_ui()
        self.setup_threads()

        exit_shortcut = QShortcut(QKeySequence("Ctrl+C"), self)
        exit_shortcut.activated.connect(self.close_application)

    def close_application(self):
        print("Ctrl+С was pressed inside the application!")
        self.close()

    def init_ui(self):
        self.setWindowTitle("News App")
        self.setGeometry(200, 200, 600, 400)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        self.news_display = QTextBrowser()
        self.layout.addWidget(self.news_display)

    def setup_threads(self):
        self.washington_post_updater = NewsUpdater(self.washington_post_scraper, 60)
        self.ny_times_updater = NewsUpdater(self.ny_times_scraper, 60)
        self.time_times_updater = NewsUpdater(self.time_times_scraper, 60)

        self.washington_post_updater.news_data_ready.connect(self.update_washington_post)
        self.ny_times_updater.news_data_ready.connect(self.update_ny_times)
        self.time_times_updater.news_data_ready.connect(self.update_time_times)

        self.washington_post_updater.start()
        self.ny_times_updater.start()
        self.time_times_updater.start()

    def update_washington_post(self, data):
        self.news_display.append(f"Washington Post:\nTitle: {data['Title']}\nAnnotation: {data['Annotation']}\nAuthors: {data['Authors']}\n---\n")

    def update_ny_times(self, data):
        self.news_display.append(f"New York Times:\nTitle: {data['Title']}\nAnnotation: {data['Annotation']}\nAuthors: {data['Authors']}\n---\n")

    def update_time_times(self, data):
        for article in data:
            self.news_display.append(f"Time Times:\nTitle: {article['Title']}\nAnnotation: {article['Annotation']}\nAuthors: {article['Authors']}\n---\n")

    from PyQt5.QtCore import QObject

    class NewsUpdater(QObject):
        news_data_ready = pyqtSignal(object)

        def __init__(self, scraper, interval):
            super().__init__()
            self.scraper = scraper
            self.interval = interval
            self.timer = QTimer(self)
            self.timer.timeout.connect(self.background_task)

        def background_task(self):
            data = self.scraper.scrape()
            self.news_data_ready.emit(data)

        def start(self):
            self.timer.start(self.interval * 1000)
            self.background_task()

def run_app():
    app = QApplication(sys.argv)
    window = NewsApp()

    def quit_app():
        app.quit()

    signal.signal(signal.SIGINT, lambda *_: quit_app())
    signal.signal(signal.SIGTERM, lambda *_: quit_app())

    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    run_app()